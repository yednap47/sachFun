# Id: sachFun.jl, Mon 27 Jun 2016 11:15:11 AM MDT pandeys #
# Created by Sachin Pandey, LANL
# Description: Miscellaneous functions that I use.
#------------------------------------------------------------------
module sachFun
using YAML
using HDF5
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
import DataFrames
df = DataFrames

#------------------------------------------------------------------
function test()
  println("hello world")
end

#------------------------------------------------------------------
function readObsHeaders(filename)
  # Open observation file and grab values for variables of interest
  # e.g.:
  # filename = "./pflotran-obs-0.tec"

  # open file
  f = HDF5.open(filename)
  test = HDF5.readlines(f)
  HDF5.close(f)

  # get headers
  headers = split(test[1],"\"")[2:2:end-1]

  return headers
end

function readh5headers(filename)
  f = HDF5.h5open(filename,"r")
  h5dict = HDF5.read(f)
  HDF5.close(f)
  keyarray = collect(keys(h5dict))
  timekeyarray = filter(key -> split(key)[1] == "Time:", keyarray)
  headers = collect(keys(h5dict[timekeyarray[1]]))
  return headers
end

function readObsDataset(filename,myvar;dataframe=false)
  # Open observation file and grab values for variables of interest
  # e.g.:
  # filename = "./pflotran-obs-0.tec"
  # myvar =  ["pH obs","Total H+", "Free H+", "OH-"]
  # times,conc = readObsDataset(filename,myvar)

  # open file
  f = open(filename)
  test = readlines(f)
  close(f)

  # get headers
  headers = split(test[1],"\"")[2:2:end-1]

  # grab index associated with myvar
  # j = variables we care about
  # i = times in the observation file
  myvali = Array(Int,length(myvar),1)
  for j = 1:length(myvar)
    for i in 1:length(headers)
      if contains(headers[i],myvar[j])
        myvali[j] = parse(Int,split(split(headers[i])[1],"-")[1])
      end
    end
  end

  # grab times and values
  times = zeros(length(test)-1,1)
  myval = Array(Float64,length(test)-1,length(myvali)+1)
  for j in 1:length(myvali)
    for i in 2:length(test)
      temp = split(test[i])
      times[i-1,:] = parse(Float64, temp[1])
      try 
          myval[i-1,j+1] = parse(Float64, temp[myvali[j]])
      catch
          warn("readObsDataset, invalid number for $(myvar[j])! Setting to zero")
          myval[i-1,j+1] = 0.0
      end
    end
  end
  myval[:,1] = times

  if dataframe
      myval = df.DataFrame(myval)
      labels = vcat("Time",myvar)
      df.names!(myval, [Symbol(labels[i]) for i in 1:length(labels)])
  end

  return myval
end

#------------------------------------------------------------------
function readh5batch(filename,myvar)
  # Open h5 file and create dictionary and make dictionary for the h5 file navigation, e.g.:
  # Dict{ByteString,Any} with 14 entries:
  #   "Time:  7.00000E+01 d" => Dict{ByteString,Any}("Total_NO2- [M]"=>1x1x1 Array{Float64,3}:…
  # ...
  #   "Provenance"           => Dict{ByteString,Any}("PETSc"=>Dict{ByteString,Any}("detail_petsc_status"=>ASCIIString…
  fid = h5open(filename,"r")
  h5dict = read(fid) # apparently when you read h5 files it reads it as a dictionary
  close(fid)

  # grab all of the "names" of the different "folders" in the h5 file
  # collect: "Return an array of all items in a collection."
  # keys: Return an iterator over all keys in a collection.
  # e.g.:
  # 14-element Array{ByteString,1}:
  #  "Time:  7.00000E+01 d"
  keyarray = collect(keys(h5dict))

  # An array of the strings that are associated with different times, as written in outputfile, e.g:
  # Only has the times, not "coordinates" or provenance"
  # 12-element Array{ByteString,1}:
  #  "Time:  7.00000E+01 d"
  # ...
  timekeyarray = filter(key -> split(key)[1] == "Time:", keyarray)

  # Now turn the times into floats and sort
  timearray = [parse(Float64,split(key)[2]) for key in timekeyarray]
  sta = sort(timearray) # this is what is used to grab the associated variable values in h5dict

  # keydict is a dictionary: time (float) => string in output file (string)
  keydict = Dict(zip(timearray,timekeyarray))

  # now find the variable you are interested in (using first time entry)
  varkeyarray = collect(keys(h5dict[timekeyarray[1]]))
  myvar_fullname=Array(AbstractString,length(myvar))
  for j = 1:length(myvar)
    for i in 1:length(varkeyarray)
      if contains(varkeyarray[i],myvar[j])
        myvar_fullname[j] = varkeyarray[i]
      end
    end
  end

  # ERROR: UndefRefError: access to undefined reference
  # in getindex at ./array.jl:282
  # -> This means there is something wrong with your variable names in myvar

  # now grab the values for each variable for all times
  # TIME IS THE FIRST COLUMN
  myval = Array(Float64,length(timekeyarray),length(myvar)+1)
  myval[:,1] = timearray
  for j = 1:length(myvar)
    for i in 1:length(sta)
      myval[i,j+1] = h5dict[timekeyarray[i]][myvar_fullname[j]][1]
    end
  end
  myval = sortrows(myval)

  return myval
end

#------------------------------------------------------------------
function readh5_1D(filename,myvar,coord_name,mytime)
  # Open h5 file and create dictionary and make dictionary for the h5 file navigation, e.g.:
  # Dict{ByteString,Any} with 14 entries:
  #   "Time:  7.00000E+01 d" => Dict{ByteString,Any}("Total_NO2- [M]"=>1x1x1 Array{Float64,3}:…
  # ...
  #   "Provenance"           => Dict{ByteString,Any}("PETSc"=>Dict{ByteString,Any}("detail_petsc_status"=>ASCIIString…
  fid = h5open(filename,"r")
  h5dict = read(fid) # apparently when you read h5 files it reads it as a dictionary
  close(fid)

  # grab coordinates
  coord_fullname=Array(AbstractString,1)
  coordinates = collect(keys(h5dict["Coordinates"]))
  for i in 1:length(coordinates)
    if contains(coordinates[i],coord_name)
      coord_fullname = coordinates[i]
    end
  end

  # time and coordinate stuff
  my_coord = h5dict["Coordinates"][coord_fullname][2:end]
  h5dict["Coordinates"]["X [m]"][2:end]
  keyarray = collect(keys(h5dict))
  timekeyarray = filter(key -> split(key)[1] == "Time:", keyarray)
  timearray = [parse(Float64,split(key)[2]) for key in timekeyarray]
  sta = sort(timearray) # this is what is used to grab the associated variable values in h5dict

  # keydict is a dictionary: time (float) => string in output file (string)
  keydict = Dict(zip(timearray,timekeyarray))

  # now find the variable you are interested in (using first time entry)
  varkeyarray = collect(keys(h5dict[timekeyarray[1]]))
  myvar_fullname=Array(AbstractString,length(myvar))
  for j = 1:length(myvar)
    for i in 1:length(varkeyarray)
      if contains(varkeyarray[i],myvar[j])
        myvar_fullname[j] = varkeyarray[i]
      end
    end
  end

  # grab the data
  myval = Array(Float64,length(my_coord),length(myvar)+1)
  myval[:,1] = my_coord
  for i = 1:length(myvar)
      myval[:,i+1] = h5dict[keydict[mytime]][myvar_fullname[i]][:]
  end
  return myval

end

function readh5_2D(filename,myvars,mytimestep;reshaped=false)
    f = HDF5.h5open(filename,"r")
    h5dict = HDF5.read(f)
    HDF5.close(f)

    keyarray = collect(keys(h5dict))
    timekeyarray = filter(key -> split(key)[1] == "Time:", keyarray)
    timearray = [parse(Float64,split(key)[2]) for key in timekeyarray]
    sta = sort(timearray)
    keydict = Dict(zip(timearray,timekeyarray))

    results = Dict()
    results["xcoords"] = h5dict["Coordinates"]["X [m]"][1:end-1]
    results["ycoords"] = h5dict["Coordinates"]["Y [m]"][1:end-1]
    results["nx"] = length(results["xcoords"])
    results["ny"] = length(results["ycoords"])

    for myvar in myvars
        # get concentrations
        # results[myvar] = Array{Float64}(results["nx"]*ny,length(myvar))
        results[myvar] = h5dict[keydict[sta[mytimestep]]][myvar][:]
        

        results["xgrid"] = Array(Float64,0)
        results["ygrid"] = Array(Float64,0)
        for i in 1:results["nx"]
            for j in 1:results["ny"]
                append!(results["xgrid"],results["xcoords"][i])
                append!(results["ygrid"],results["ycoords"][j])
            end
        end

        if reshaped
            results[myvar] = reshape(results[myvar],results["ny"],results["nx"])
        end
    end
    
    return results
end


#------------------------------------------------------------------
function readh5_1D_obs(filename,myvar,coord_name,distance)
  # Open h5 file and create dictionary and make dictionary for the h5 file navigation, e.g.:
  # Dict{ByteString,Any} with 14 entries:
  #   "Time:  7.00000E+01 d" => Dict{ByteString,Any}("Total_NO2- [M]"=>1x1x1 Array{Float64,3}:…
  # ...
  #   "Provenance"           => Dict{ByteString,Any}("PETSc"=>Dict{ByteString,Any}("detail_petsc_status"=>ASCIIString…
  fid = h5open(filename,"r")
  h5dict = read(fid) # apparently when you read h5 files it reads it as a dictionary
  close(fid)

  # grab coordinates
  coord_fullname=Array(AbstractString,1)
  coordinates = collect(keys(h5dict["Coordinates"]))
  for i in 1:length(coordinates)
    if contains(coordinates[i],coord_name)
      coord_fullname = coordinates[i]
    end
  end

  # time stuff
  keyarray = collect(keys(h5dict))
  timekeyarray = filter(key -> split(key)[1] == "Time:", keyarray)
  timearray = [parse(Float64,split(key)[2]) for key in timekeyarray]
  sta = sort(timearray) # this is what is used to grab the associated variable values in h5dict

  # keydict is a dictionary: time (float) => string in output file (string)
  keydict = Dict(zip(timearray,timekeyarray))

  # now find the variable you are interested in (using first time entry)
  varkeyarray = collect(keys(h5dict[timekeyarray[1]]))
  myvar_fullname=Array(AbstractString,length(myvar))
  for j = 1:length(myvar)
    for i in 1:length(varkeyarray)
      if contains(varkeyarray[i],myvar[j])
        myvar_fullname[j] = varkeyarray[i]
      end
    end
  end

  # index of the distance we want, have to remove "rounding errors"
  distance_ctr = find(x -> round(x,8) == distance,h5dict["Coordinates"][coord_fullname])[1]

  # grab the data
  myval = Array(Float64,length(keydict),length(myvar)+1)
  myval[:,1]=timearray
  for j = 1:length(myvar)
    for i = 1:length(timearray)
        myval[i,j+1] = h5dict[keydict[timearray[i]]][myvar_fullname[j]][distance_ctr]
    end
  end
  myval = sortrows(myval)
  return myval
end

#------------------------------------------------------------------
function readh5_1D_domainAverage(filename,myvar,coord_name)

  # Example arguments:
  # filename = "1d_flow_ds2o4dt_dhcro4dt.h5"
  # myvar = ["Fe(OH)3_VF","Siderite_VF","Cr(OH)3(s)_VF"]
  # coord_name = "X"
  # timeunits = "days"

  fid = h5open(filename,"r")
  h5dict = read(fid) # apparently when you read h5 files it reads it as a dictionary
  close(fid)

  # grab coordinates
  coord_fullname=Array(AbstractString,1)
  coordinates = collect(keys(h5dict["Coordinates"]))
  for i in 1:length(coordinates)
    if contains(coordinates[i],coord_name)
      coord_fullname = coordinates[i]
    end
  end

  # time and coordinate stuff
  my_coord = h5dict["Coordinates"][coord_fullname][2:end]
  h5dict["Coordinates"]["X [m]"][2:end]
  keyarray = collect(keys(h5dict))
  timekeyarray = filter(key -> split(key)[1] == "Time:", keyarray)
  timearray = [parse(Float64,split(key)[2]) for key in timekeyarray]
  sta = sort(timearray) # this is what is used to grab the associated variable values in h5dict

  # keydict is a dictionary: time (float) => string in output file (string)
  keydict = Dict(zip(timearray,timekeyarray))

  # now find the variable you are interested in (using first time entry)
  varkeyarray = collect(keys(h5dict[timekeyarray[1]]))
  myvar_fullname=Array(AbstractString,length(myvar))
  for j = 1:length(myvar)
    for i in 1:length(varkeyarray)
      if contains(varkeyarray[i],myvar[j])
        myvar_fullname[j] = varkeyarray[i]
      end
    end
  end

  myval = Array(Float64,length(keydict),length(myvar)+1)
  myval[:,1]=timearray
  temp = Array(Float64,length(my_coord))
  for j in 1:length(myvar_fullname)
    for i in 1:length(timearray)
      # grab data in each cell
      temp = h5dict[keydict[timearray[i]]][myvar_fullname[j]][:]
      myval[i,j+1] = mean(temp)
    end
  end
  myval = sortrows(myval)
  return myval
end


function plot_obs(obs,case,myvar,runfile=false,pfle_dir=" ")
    if runfile
        # assign names for *.h5, *.out, calibration target names
        obstag = case * "-obs-0.tec"
        otag = case * ".out"
        htag = case * ".h5"

        # remove old .h5 and .out files
        run(`rm -f $obstag`)
        run(`rm -f $otag`)
        run(`rm -f $htag`)
        
        # run the file
        run(`$pfle_dir -pflotranin $case.in`)
    end

    results = sachFun.readObsDataset("$case-obs-0.tec",myvar)

    nvar = length(myvar)
    subplot_dim = Int(ceil(sqrt(nvar)))
    fig = figure("pyplot",figsize=(subplot_dim*5,subplot_dim*5))
    majorFormatter = matplotlib[:ticker][:FormatStrFormatter]("%0.2e")
    for i in 1:nvar
        subplot(subplot_dim,subplot_dim,i)
        p = plot(results[:,1],results[:,i+1],linestyle="-",
            marker=" ",
            label= myvar[i],
            color = "b") # Plot a basic line
        legend(loc=0,frameon=false)
        ax=gca()
        ax[:yaxis][:set_major_formatter](majorFormatter)
    end
    tight_layout()
end

#------------------------------------------------------------------
function WaterLevelBadDates(yamlfile)   
    # go through water level data and find bad dates (i.e. large skips in data
    # from one directory above, e.g:
    # WaterLevelBadDates("R-63i/R-63i.yaml")
    fname = split(yamlfile,"/")[end]
    wellname = split(fname,".")[1]
    welldir =  replace(yamlfile,fname,"")
    
    println("finding gaps in water levels for $wellname")
    yamldata = YAML.load(open(joinpath(welldir,fname)))
    observationwell = yamldata["Observation well"]
    observationbegintime = yamldata["Observation begin time"]
    endtime = yamldata["End time"]
    
    portdescr=[]
    try
      portdescr = string(yamldata["Screen number"])
    catch
      portdescr = "SINGLE COMPLETION"
    end    

    db.connecttodb()    
    waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)
    db.disconnectfromdb()

    gaplimit = Base.Dates.Millisecond(Base.Dates.Day(5))
    gapstart = []
    gapend = []
    println("Bad dates:")
    for i in 2:length(waterleveltimes)
        timedelta = waterleveltimes[i]-waterleveltimes[i-1]
        if timedelta > gaplimit
            gapstart = Dates.format(round(waterleveltimes[i-1]-Base.Dates.Hour(1), Dates.Minute(60)), "yyyy-mm-dd HH:MM:SS")
            gapend = Dates.format(round(waterleveltimes[i]+Base.Dates.Hour(1), Dates.Minute(60)), "yyyy-mm-dd HH:MM:SS")
            println("- ['$gapstart', '$gapend']")
        end
    end
end

function rawWaterLevels(basedir,yamlfile)
    fname = split(yamlfile,"/")[end]
    wellname = split(fname,".")[1]
    welldir =  replace(yamlfile,fname,"")
        
    println("getting raw water levels for $wellname")
    yamldata = YAML.load(open(joinpath(basedir,welldir,fname)))
    observationwell = yamldata["Observation well"]
    observationbegintime = yamldata["Observation begin time"]
    endtime = yamldata["End time"]
    
    portdescr=[]
    try
      portdescr = string(yamldata["Screen number"])
    catch
      portdescr = "SINGLE COMPLETION"
    end    

    db.connecttodb()    
    waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)
    db.disconnectfromdb()
    
    return waterleveltimes,waterlevels
end

function denoiseWaterLevels(yamlfile)
    dateform = Dates.DateFormat("y-m-d H:M:S") # to convert yaml dates to datetime
    fname = split(yamlfile,"/")[end]
    wellname = split(fname,".")[1]
    welldir =  replace(yamlfile,fname,"")

    println("getting water levels for $wellname")

    #------------------------------------------------------------------------------
    # Initialization
    #------------------------------------------------------------------------------
    yamldata = YAML.load(open(joinpath(welldir,fname)))
    observationwell = yamldata["Observation well"]
    observationbegintime = yamldata["Observation begin time"]
    endtime = yamldata["End time"]

    portdescr=[]
    try
      portdescr = string(yamldata["Screen number"])
    catch
      portdescr = "SINGLE COMPLETION"
    end

    baddates = []
    try
      baddates = yamldata["Bad dates"]
    catch
    end

    jumpfixdates = []
    jumpfixsize = []
    try
      jumpfixdates = map((x) -> DateTime(x[1],dateform), yamldata["Observation jumps"])
      jumpfixsize  = map((x) -> x[2], yamldata["Observation jumps"])
    catch
    end

    samplethreshold=[]
    try
      samplethreshold = controldata["Sample threshold"]
    catch
      samplethreshold = 0.08
    end

    samplelength=[]
    try
        samplelength = controldata["Sample length"]
    catch
        samplelength = 5
    end

    barometricbegintime = Dates.format(DateTime(observationbegintime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
    barometricendtime = Dates.format(DateTime(endtime,dateform)+Dates.Day(1),"yyyy-mm-dd")

    #------------------------------------------------------------------------------
    # Data processing
    #------------------------------------------------------------------------------
    db.connecttodb()

    # get the water level information
    waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)

    # remove the bad dates
    for baddate in baddates    
        baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
        goodwaterlevels = []
        goodwaterleveltimes = []
      for j in 1:length(waterlevels)
        if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
        else
          push!(goodwaterlevels,waterlevels[j])
          push!(goodwaterleveltimes,waterleveltimes[j])    
        end
      end
      waterlevels = goodwaterlevels
      waterleveltimes = goodwaterleveltimes
    end

    # fix the jumps in the water levels
    j = 1
    for jumpfixdate in jumpfixdates
      i = 1
      while i < length(waterleveltimes) && waterleveltimes[i] < jumpfixdate
        i += 1
      end
      while i < length(waterleveltimes)
        waterlevels[i] += jumpfixsize[j]
        i += 1
      end
      j += 1
    end

    # get the barometric information
    baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)

    # get the earth tide information -- for now, just read it from a file
    # TODO: 
    # For some reason, db.getearthtide truncates the last date when called from julia
    earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)

    waterlevelsnosamp, waterleveltimesnosamp = cb.removeSamplingEvents(waterlevels, waterleveltimes, threshold=samplethreshold, samplinglength=samplelength)
    nosampwaterlevels, nosampwaterleveltimes = cb.denoise(waterlevelsnosamp, waterleveltimesnosamp, baropressmb, barotimes, earthtideforces, earthtidetimes)
    waterlevels = nosampwaterlevels
    waterleveltimes = nosampwaterleveltimes

    # remove the bad dates from the no samp water levels
    for baddate in baddates
        baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
        goodwaterlevels = []
        goodwaterleveltimes = []
        for j in 1:length(waterlevels)
            if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
            else
                push!(goodwaterlevels,waterlevels[j])
                push!(goodwaterleveltimes,waterleveltimes[j])    
            end
        end
        waterlevels = goodwaterlevels
        waterleveltimes = goodwaterleveltimes
    end

    db.disconnectfromdb()

    @show minimum(waterleveltimes)
    @show maximum(waterleveltimes)
    return waterleveltimes,waterlevels
end

function dbPumpingRates(productionwells, begintime, endtime)
    db.connecttodb()
    #get the production information
    production = Dict()
    for productionwell in productionwells
        i = find(productionwells .== productionwell)[1]
        production[productionwell] = Dict()
        production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, begintime, endtime)
        production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
    end
    db.disconnectfromdb()
    return production
end # end module

function getObsCharges(fname, obsname)
    # WARNING: MUST HAVE COLUMN IDs OFF!!

    chemnames = getChemNames(fname,obsname)
    chemnames = map(x->split(x," [M]")[1],chemnames) # remove obs info
    chemnames = map(x->split(x," [M]")[1],chemnames) # remove obs info

    for i in 1:length(chemnames)
        if contains(chemnames[i],"Free")
            chemnames[i] = split(chemnames[i],"Free ")[2]
        end
    end

    charges=Array{Float64}(0)
    for i in 1:length(chemnames)
        if contains(chemnames[i],"H2O")
            append!(charges,0.0)
        elseif contains(chemnames[i],"(aq)")
            append!(charges,0.0)
        elseif contains(chemnames[i],"(5+)")
            append!(charges,5.0)
        elseif contains(chemnames[i],"++++")
            append!(charges,4.0)
        elseif contains(chemnames[i],"+++")
            append!(charges,3.0)
        elseif contains(chemnames[i],"++")
            append!(charges,2.0)
        elseif contains(chemnames[i],"+")
            append!(charges,1.0)
        elseif contains(chemnames[i],"(5-)")
            append!(charges,-5.0)
        elseif contains(chemnames[i],"----")
            append!(charges,-4.0)
        elseif contains(chemnames[i],"---")
            append!(charges,-3.0)
        elseif contains(chemnames[i],"--")
            append!(charges,-2.0)
        elseif contains(chemnames[i],"-")
            append!(charges,-1.0)
        else
            @show chemnames[i]
            error("blah")
        end
    end
    
    return charges
end

function getChemNames(fname,obsname)
    rawnames = sachFun.readObsHeaders(fname)
    rawnames = rawnames[map(x->contains(x," $(obsname) "),rawnames)]
    rawnames = rawnames[map(x->contains(x,"Liquid Pressure"),rawnames).!=true] # remove pressure
    rawnames = rawnames[map(x->contains(x,"Liquid Saturation"),rawnames).!=true] # remove 
    rawnames = rawnames[map(x->contains(x,"Total"),rawnames).!=true] # remove totals
    rawnames = rawnames[map(x->contains(x,"VF"),rawnames).!=true] # remove VF
    rawnames = rawnames[map(x->contains(x,"Rate"),rawnames).!=true] # remove Rates
    rawnames = rawnames[map(x->contains(x,"[mol/m^3]"),rawnames).!=true] # remove stuff in immobile phase
    chemnames = rawnames[map(x->contains(x,"pH"),rawnames).!=true] # remove pH  
    return chemnames
end

function getChemNamesTotal(fname,obsname)
    chemnames = sachFun.readObsHeaders(fname)
    chemnames = chemnames[map(x->contains(x,"Total"),chemnames).==true] # remove totals
    return chemnames
end

function getChemNamesMnrl(fname,obsname)
    chemnames = sachFun.readObsHeaders(fname)
    chemnames = chemnames[map(x->contains(x,"VF"),chemnames).==true] # remove totals
    return chemnames
end

# function dbWaterLevels(wellname, begintime, endtime)
#     db.connecttodb()
#     #get the production information
#     production = Dict()
#     for productionwell in productionwells
#         i = find(productionwells .== productionwell)[1]
#         production[productionwell] = Dict()
#         production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, begintime, endtime)
#         production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
#     end
#     db.disconnectfromdb()
#     return production
# end # end module


function calc_rsquared(observed,modeled)
    if length(observed) != length(modeled)
        error("length of targets does not equal length equal predicted!")
    end
    ybar = mean(observed)
    SStot = sum(map(x->(x-ybar)^2,observed))
    SSres = sum((observed-modeled).^2)
    rsquared = 1-SSres/SStot
    return rsquared, SSres
end

end
